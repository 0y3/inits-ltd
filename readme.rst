

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.
wamp, mamp or xampp

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************
- Download and copy folder to your htdoc folder in your wamp, mamp or xampp location on your OS.
- Extract the 'inits.sql' database in the 'db' folder to your phpmyadmin sql .
- Load the the site using the folder name "eg http://localhost/inits/"
- For the admin, 'http://localhost/inits/admin'
- Email: admin@admin.com 
- Password: 123456


***************
Acknowledgement
***************

The CodeIgniter team would like to thank Trovolink, all the
contributors to the CodeIgniter project and you, the CodeIgniter user.
