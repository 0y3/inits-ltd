<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Businesses extends CI_Model
{
    
	function __construct()
	{
		parent::__construct();
	}

    function queryParameters($params=array())
    {
        // filter by Biz status
        if(isset($params['bizstatus']) && isset($params['bizstatus'])){
            $this->db->where(array('tbl_biz.status'=>$params['bizstatus']));
        }

        // filter by Biz id
        if(isset($params['bizid']) && isset($params['bizid'])){
            $this->db->where(array('tbl_biz.id'=>$params['bizid']));
        }

        // filter by merchant slug
        if(isset($params['bizslug']) ){
           $this->db->where(array('tbl_biz.slug'=>$params['bizslug']));
        }

        // filter by Biz name like
        if(isset($params['biznamelike']) ){
            $this->db->where("(tbl_biz.bizname LIKE'%$params[biznamelike]%')");
        }


        // filter by Cate name like
        if(isset($params['catenamelike']) ){
            $this->db->where("(tbl_cate.catename LIKE'%$params[catenamelike]%')");
        }


        // filter by Cate Biz 
        if(isset($params['catebiz_bizid']) && isset($params['catebiz_bizid'])){
           $this->db->where(array('tbl_catebiz.bizid'=>$params['catebiz_bizid']));
        }

        // filter by Cate Biz 
        if(isset($params['catebiz_cateid']) && isset($params['catebiz_cateid'])){
           $this->db->where(array('tbl_catebiz.cateid'=>$params['catebiz_cateid']));
        }
    }
	
	function getAll($param=array(), $limit_start=null)
	{
		$this->db->select('*');
		$this->db->from('tbl_biz');

        $this->queryParameters($param);

		$this->db->where(array('isdeleted'=>0 )
            );
        $this->db->order_by("tbl_biz.createdat", 'DESC');

        // Perform checks to show merchants only orders that belong to their store
        
		$query = $this->db->get();
        $get_impact = null;
        if($query->num_rows() > 0)
        {
            //return $query->result_array();
            $rows = $query->result_array();
            foreach($rows as $row)
            {
                $row['biz_cate']  =   $this->getCateBizById(array('catebiz_bizid'=>$row['id'] ));
                $get_impact[] = $row;
            }
            
            return $get_impact;
        }
        else
        {
            return null;
        }
	}

    Public function getAllCount($param=array(), $limit_start=null)
    {
        $this->db->select('*');
        $this->db->from('tbl_biz');

        $this->queryParameters($param);
        // Clause to only fetch data with deletedat field set to null
        $this->db->where(
                        array(
                            'isdeleted'=>0
                        )
                    );
        $total = $this->db->count_all_results();
        return $total;
    }

    Public function getCateBizById($param=array(),$limit_start=null)
    {
        $this->db->select('tbl_catebiz.id, tbl_catebiz.cateid, tbl_cate.catename, tbl_catebiz.bizid, tbl_biz.slug as bizslug, tbl_biz.bizname');
        $this->db->join('tbl_biz', 'tbl_biz.id = tbl_catebiz.bizid', "left");
        $this->db->join('tbl_cate', 'tbl_cate.id = tbl_catebiz.cateid', "left");
        $this->db->from('tbl_catebiz');
        
        // Process any filter options if any
        $this->queryParameters($param);
        // Clause to only fetch data with isdeleted field set to zero

        $this->db->where(
                            array(
                                'tbl_catebiz.isdeleted'=>0,
                                'tbl_catebiz.status'=> 1
                            )
                        );
        
        $this->db->order_by("tbl_cate.catename", "asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return false;
        }
    }


    function getAllCate($param=array(), $limit_start=null)
    {
        $this->db->select('*');
        $this->db->from('tbl_cate');

        $this->queryParameters($param);

        $this->db->where(array('isdeleted'=>0 )
            );
        $this->db->order_by("tbl_cate.catename", 'ASC');

        // Perform checks to show merchants only orders that belong to their store
        
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return null;
        }
    }

    Public function getAllCateCount($param=array(), $limit_start=null)
    {
        $this->db->select('*');
        $this->db->from('tbl_cate');

        $this->queryParameters($param);
        // Clause to only fetch data with deletedat field set to null
        $this->db->where(
                        array(
                            'isdeleted'=>0
                        )
                    );
        $total = $this->db->count_all_results();
        return $total;
    }


    function getAllCateByBiz($param=array(), $bizid)
    {
        $this->db->select('*');
        $this->db->from('tbl_cate');

        $this->queryParameters($param);

        $this->db->where(array('isdeleted'=>0 )
            );
        $this->db->order_by("tbl_cate.catename", 'ASC');

        // Perform checks to show merchants only orders that belong to their store
        
        $query = $this->db->get();
        $get_impact = null;
        if($query->num_rows() > 0)
        {
            //return $query->result_array();
            $rows = $query->result_array();
            foreach($rows as $row)
            {
                //$row['checkIt']  =   $this->getCateBizById(array('catebiz_bizid'=>$row['id'] ));
                 $status = $this->CateAssignmentBiz(array('catebiz_bizid'=>$bizid,'catebiz_cateid'=>$row['id']));
                if($status == true)
                    $row['checkIt'] = 'yes';
                else
                    $row['checkIt'] = 'no';
                $get_impact[] = $row;
            }
            
            return $get_impact;
        }
        else
        {
            return null;
        }
    }

    function CateAssignmentBiz($param=array())
    {
        $this->db->select('*');
        $this->db->from('tbl_catebiz');
        $this->queryParameters($param);
        // Clause to only fetch data with deletedat field set to null
        $this->db->where(
                        array(
                            'isdeleted'=>0
                        )
                    );
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return true;
        else
            return false;
    }
    
    public function _get_count($by_id) 
    {
        $this->db->select("id");
        $this->db->from('tbl_catebiz');
        $this->db->where("bizid ", $by_id);
        $this->db->where("status ", 1);
        $this->db->where("isdeleted ", 0);
        $this->db->order_by("order_set", 'ASC');
        $total = $this->db->count_all_results();
        return $total;
        //$query = $this->db->get();
        //return $query->result_array();
    }
    function getPhotoById($id=null)
	{
        $this->db->select("*");
        $this->db->from('tbl_cate');
        $this->db->where( array(
                                'bizid' => (int)$id,
                                'status' => 1,
                                'isdeleted' =>'0'
                                )
                         );
        $this->db->order_by("order_set", 'ASC');
        $this->db->order_by("createdat", 'DESC');
        $query = $this->db->get();
            if ($query->num_rows() > 0)
            {
                    //$row = $query->row();
                    return $query->result_array();
            }
            else
                    return false;
	}
       
        
        public function merchant_info($by_id) 
        {

            $this->db->select(" ".$this->DB_merchant.".*  , ".$this->DB_city.".cityname , ".$this->DB_state.".statename")
                     ->from($this->DB_merchant)
                     ->where(array(" ".$this->DB_merchant.".id" => (int)$by_id ," ".$this->DB_merchant.".isdeleted" =>"0"))
                     ->join( $this->DB_city, " ".$this->DB_city.".id = ".$this->DB_merchant.".cityid " ,"LEFT ")
                     ->join( $this->DB_state, " ".$this->DB_state.".id = ".$this->DB_merchant.".stateid ","LEFT ");

            $query = $this->db->get()->row();
            //print("<pre>".print_r($query,true)."</pre>");die;
            return $query;

        }

        public function admin_info() 
        {

            $this->db->select(" *")
                     ->from('settings')
                     ->where(" id" ,1);

            $query = $this->db->get()->row();
            //print("<pre>".print_r($query,true)."</pre>");die;
            return $query;

        }
        
    function getAllAdmin($userid=null)
    {
        $this->db->select('admin_users.*, user_roles.roleName');
        $this->db->from('admin_users');
        $this->db->join('user_roles', 'admin_users.userroleid = user_roles.id');
        $this->db->where(array('admin_users.isdeleted'=>0));

        // Perform checks to show merchants only orders that belong to their store
        
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else{
            return false;
        }
    }

    public function userroleAdmin()
        {
            $this->db->select('*');
            $this->db->from('user_roles');
            $this->db->where_in("roleFor ", 'ebs');
            $this->db->where("status ", 1);
            $this->db->order_by("createdat" , "DESC");

            $query = $this->db->get()->result();

            //print("<pre>".print_r($query,true)."</pre>");die;
            return $query;

        }
	
}
