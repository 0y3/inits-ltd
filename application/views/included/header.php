        <header class="sm_header">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 logo-column text-center">
                        <a href="<?= site_url() ?>">
                            <img src="<?= base_url() ?>assets/img/inits.png" alt="logo">
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 pt-5 nav-column clearfix">
                        <nav id="menu" class="d-none d-lg-block">
                            <ul class="text-center">                                
                                <li class="sitenav home "><a href="<?= site_url('')?>">Home</a></li>
                                <li class="sitenav"><a target="_blank" href="<?= site_url('admin')?>">Log</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>        