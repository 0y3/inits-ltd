<footer>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 header-social text-justify">
                    <a target="_blank" href="https://www.twitter.com/" class="fa fa-twitter"></a>
                    <a target="_blank" href="https://www.instagram.com/" class="fa fa-instagram"></a>
                    <a target="_blank" href="javascript:void(0)" class="fa fa-youtube"></a>
                </div>
                <div class="col-lg-9 ">

                    <div class="footer-nav text-left">
                        <a href="<?= site_url('')?>">Home</a>
                        <a target="_blank" href="<?= site_url('admin')?>">Log</a>
                    </div>
                    
                </div>
                <div class="col-lg-3 text-right">
                    <div class="copyright-txt">
                        © <?php echo date('Y'); ?> Classic Business Service<!--Trovolink tech. All Rights Reserved.-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>