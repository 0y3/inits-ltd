

    <div class=" col-md-12">
                <?php
                    if(isset($bizinfo))
                    { 
                        $action='edit/'.$bizinfo['id'];
                        $savename='Update';
                    }
                    else
                    { 
                        $action='save';
                        $savename='Save';

                    }
                ?>
        <div class="card card-default">
            <form id="" action="<?= site_url('admin/business/'.$action) ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title">Business Details</h3>
                </div>
                <div class="col-md-12">
                    <div class="col-md-8 col-md-offset-2 alert alert-danger alert-dismissable get_error" style="display: none;">

                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>

                        <span class="error_msgr_lg"> </span>

                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="save_name">Business Name</label>
                        <input type="text" id=""  name="name" class="form-control" placeholder="Enter Business Name" value="<?php if(isset($bizinfo)) echo $bizinfo['bizname']?>" required='' >
                    </div>

                    <div class="form-group">
                        <label>Short Description</label>
                        <textarea class="form-control" name="description" rows="2"placeholder="Enter Business Short Description"><?php if(isset($bizinfo)) echo $bizinfo['description']?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" name="address" rows="2"placeholder="Enter Business Address"><?php if(isset($bizinfo)) echo $bizinfo['address']?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="save_name"> Email</label>
                        <input type="email" id=""  name="email" class="form-control" placeholder="Enter Business Email" value="<?php if(isset($bizinfo)) echo $bizinfo['email']?>" required='' >
                    </div>
                    <div class="form-group">
                        <label for="save_name"> Phone</label>
                        <input type="number" id=""  name="phone" class="form-control" placeholder="Enter Business Phone" value="<?php if(isset($bizinfo)) echo $bizinfo['phone']?>" required='' >
                    </div>
                    <div class="form-group">
                        <label for="save_name"> Website</label>
                        <input type="url" id=""  name="website" class="form-control" placeholder="Enter Business Website" value="<?php if(isset($bizinfo)) echo $bizinfo['website']?>" >
                    </div>

                    <div class="form-group">
                        <label class="control-label">Category</label>
                        <br>
                        <select id="category_list" name="category[]" multiple="multiple" required="">

                            <?php if(!empty($cate)): ?>

                                <?php foreach ($cate as $category_list) :?>

                                    <option value="<?= $category_list['id'] ?>" <?php if(isset($category_list['checkIt']) && $category_list['checkIt']== 'yes') echo 'selected'?> > <?= $category_list['catename'] ?></option>
                                    
                                <?php endforeach;?>
                                <?php else: ?>

                                    <option > Category not available</option>

                            <?php endif; ?>

                        </select>
                    </div>
                    
                    

                    <?php if(isset($bizinfo)): ?>
                    <div class="form-group">

                        
                        <div class="col-sm-6">
                            <label>Status</label>
                            <br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="status" value="1" class="custom-control-input" required="" <?php if($bizinfo['status']==1) echo 'checked';?> >
                                <label class="custom-control-label  text-success" for="customRadioInline1">Active</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="status"  required=""  <?php if($bizinfo['status']==0) echo 'checked';?> value="0" class="custom-control-input">
                                <label class="custom-control-label text-danger" for="customRadioInline2">In-Active</label>
                            </div>
                        </div>
                        

                    </div>
                    <?php endif; ?>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success sbmtbtnn"><i class="fa fa-floppy-o"></i> &nbsp; <?= $savename?> &nbsp; </button>
                </div>
            </form>
        </div>
    </div>                    
       
        <script>
            $(document).on("click",".sbmtbtn", function(e){
               
                    $('.preloader').css("display", "block");
            });

        </script>