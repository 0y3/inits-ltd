<div class="container-fluid">
  <div class="row">
          
          <div class="col-lg-12 col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Biz List</h3>
                <div class="card-tools">
                  <a href="<?=site_url('admin/business/addform')?>" class="btn btn-block btn-info btn-sm">Add New</a>

                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0 table-responsive ">
                <table class="table table-striped table-hover table-bordered">
                  <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Website</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th> Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                      <?php if(!empty($biz)): ?>  
                      <?php $count=1; ?>
                          <?php foreach ($biz as $bizs) :?>

                              <?php 
                             
                                $status = "";
                                $label_color = array("", "danger", "warning", "primary", "success", "info");

                                if($bizs['status']==1)
                                {
                                    $status='<span class="badge bg-success">Active</span>';
                                }
                                elseif($bizs['status']==0)
                                {
                                    $status='<span class="badge bg-danger">Inactive</span>';
                                }
                                      
                              ?>
                  <tr>
                    <td><?=$count;?>.</td>
                    <td><?=$bizs['bizname'];?></td>
                    <td><?=$bizs['description'];?></td>
                    <td><?=$bizs['address'];?></td>
                    <td><a href="mailto:<?=$bizs['email'];?>"><span class="badge bg-primary"><?=$bizs['email'];?></span></a></td>
                    <td><a href="tel:<?=$bizs['phone'];?>"><span class="badge bg-primary"><?=$bizs['phone'];?></span></a></td>
                    <td><a target="_blank" href="<?=$bizs['website'];?>"><span class="badge bg-primary"><?=$bizs['website'];?></span></a></td>
                    <td>
                      <?php foreach ($bizs['biz_cate'] as $bizcate) :?>
                        <span id="<?=$bizcate['id'] ?>" class="cat_span"><i class="fa fa-caret-right mr-2"></i> <?=$bizcate['catename']?> </span><br>
                      <?php endforeach; ?>
                        
                      </td>
                    <td><?=$status;?></td>
                    <td>
                      <div class="margin btn-groupp">
                        <a href="<?=site_url('admin/business/editform/'.$bizs['slug'])?>" class="btn btn-sm btn-default"><i class="fa fa-align-left fa-edit"></i> Edit</a>
                        <a href="<?=site_url('admin/business/gallery/'.$bizs['slug'])?>" class="btn btn-sm btn-default"><i class="fa fa-picture-o"></i> View</a>
                        <a href="javascript:void(0);"  class="btn btn-sm btn-danger impact_del" data-get="<?=$bizs['id']?>"><i class="fa fa-align-right fa-trash"></i> Delete</a>
                      </div>
                    </td>
                    
                  </tr>
                  <?php $count++; ?>
                  <?php endforeach; ?>
                  <?php else: ?>
      
                      <tr>
                          <td class="text-center" colspan="10"><b>No Business Profile Created</b></td>
                      </tr>
                  <?php endif; ?>
                </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- ./col -->
  </div>
  <!-- /.row -->
</div>

<!-- Modal confirm order  -->
    <div class="modal" id="empty_confirmModal" style="display: none; ">
            <div class="modal-dialog">
                    <div class="modal-content">
                        
                            <div class="modal-body" >
                                <div class="col-sm-12 alert alert-danger" id="empty_confirmMessage"> </div>
                            </div>
                        
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" id="empty_confirmOk">Ok</button>
                                <button type="button" class="btn btn-success" id="empty_confirmCancel">Cancel</button>
                            </div>
                        
                    </div>
            </div>
    </div>

<script >
    
     //  the status process button
    $(document).on("click",".impact_del", function(e){
        e.preventDefault();
        var empty_msg = "Are you sure you want to Delete this Business?";
        var row_id = $(this).data('get'); // gets value
        
        confirmDialog(empty_msg, function(){
            
            
            $.ajax({
                type:'POST',
                url:'<?= site_url('admin/business/delete')?>',
                dataType: 'json',
                data:{
                        _id:row_id
                    },
                beforeSend: function(){
                       $('.preloader').css("display", "block");
                   },
                success:function(html){
                
                    if(html.status == '1')
                    {
                        
                    }
                    else{  }
                },
                complete:function(data){
                        // Hide image container
                        window.location.reload(); 
                        
                       }

            });
        
        }); 
        
    }); 
    
    function confirmDialog(message, onConfirm){
          var fClose = function(){
          modal.modal("hide");
          };
          var modal = $("#empty_confirmModal");
          modal.modal("show");
          $("#empty_confirmMessage").empty().append(message);
          $("#empty_confirmOk").unbind().one('click', onConfirm).one('click', fClose);
          $("#empty_confirmCancel").unbind().one("click", fClose);
        }
        
    
    
</script>