

    <div class=" col-md-6">
                <?php
                    if(isset($userinfo))
                    { 
                        $action='editaccount/'.$userinfo['id'];
                        $savename='Update';
                        $formid='cate_form';
                    }
                    else{ 
                        $action='save';
                        $savename='Save';
                        $formid='cate_form';

                    }
                ?>
        <div class="card card-primary">
            <form id="<?=$formid?>" action="<?= site_url('admin/dashboard/'.$action) ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">

                <div class="col-md-12">
                    <div class="col-md-8 col-md-offset-2 alert alert-danger alert-dismissable get_error" style="display: none;">

                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>

                        <span class="error_msgr_lg"> </span>

                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="save_name">First Name</label>
                        <input type="text" id=""  name="firstname" class="form-control" placeholder="" value="<?php if(isset($userinfo)) echo $userinfo['firstname']?>" required >
                    </div>

                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" id=""  name="lastname" class="form-control" placeholder="" value="<?php if(isset($userinfo)) echo $userinfo['lastname']?>" required >
                    </div>

                    <div class="form-group">
                        <label >Email</label>
                            <?php if(isset($userinfo)) {
                                    $name= $userinfo['email'];
                                    $readonly='readonly';
                                }
                                else {
                                    $name= '';
                                    $readonly='';
                                }
                            ?>
                        <input type="email" id=""  name="useremail" class="form-control" placeholder="" value="<?=$name?>" <?=$readonly?> required="" >
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="number" id=""  name="phone" class="form-control" placeholder="" value="<?php if(isset($userinfo)) echo $userinfo['phone']?>" required >
                    </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success sbmtbtn" ><i class="fa fa-floppy-o"></i> &nbsp; <?= $savename?> &nbsp; </button>
                </div>
            </form>
        </div>
    </div>
                               
       
        <script>
            $(document).on("click",".sbmtbtn", function(e){
                var empty = $("#cate_form .required input").filter(function() {
                    return this.value == ""; 
                });//this checks if 1 or more inputs have no value

                if(empty.length) 
                {
                    $('.preloader').css("display", "block");
                }
            });

            $("#cate_form").submit(function (e){
                
                e.preventDefault();
                var url = $(this).attr('action');
                var method = $(this).attr('method');
                var data = $(this).serialize();
                
                $.ajax({
                   url:url,
                   type:method,
                   dataType: 'json',
                   data:new FormData(this),
                   processData:false,
                   contentType:false,
                   cache:false,
                   beforeSend: function(){
                       // Show image container
                       $('.sbmtbtn').prop("disabled", true);
                       $('.preloader').css("display", "block");
                   },
                   success:function(data)
                        {
                            
                            if(data.status === '1' )
                            {
                            }
                            
                            else if(data.status === '0' )
                            {
                                
                            } 
                            
                        },
                    complete:function(data){
                        // Hide image container
                        window.location.reload(); 
                        $('.sbmtbtn').prop("disabled", false);
                        //$('.preloader').css("display", "none");
                        
                       }
                    });

            });
        </script>