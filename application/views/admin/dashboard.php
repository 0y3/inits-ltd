<div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?= $countproject ?></h3>

                <p>Business Registed</p>
              </div>
              <div class="icon">
                <i class="fa fa-upload"></i>
              </div>
              <a href="<?=site_url('admin/business')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?= $countuser?></h3>

                <p>Categories </p>
              </div>
              <div class="icon">
                <i class="fa fa-tag"></i>
              </div>
              <a href="<?=site_url('admin/business/categories')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>

          
        </div>
        <!-- /.row -->
  </div>