<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $titel ?></title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?= $meta_keyword ?>">

 

  <!-- Favicon icon -->
  <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url() ?>assets/img/hope-logo_ico144.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= base_url() ?>assets/img/hope-logo_ico114.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url() ?>assets/img/hope-logo_ico72.png">
  <link rel="apple-touch-icon-precomposed" href="<?= base_url() ?>assets/img/inits.png">
    
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/jquery.fancybox.min.css">
  <link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/css/waitMe.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url(); ?>assets/css/bootstrap-multiselect.css">

  <!-- jQuery -->
  <script src="<?= base_url() ?>assets/admin/plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

  <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
        var site_url = '<?php echo site_url(); ?>'; 
  </script>
<style type="text/css">
  .p-15
  {
    padding: 15px !important;
  }
  .m-b-10 {
    margin-bottom: 10px;
  }
  .text-white {
      color: #fff!important;
  }
  .d-flex {
      display: flex!important;
  }
  .user-dd {
      min-width: 280px!important;
  }

  /*******************
  Preloader
  ********************/
  .preloader {
    width: 100%;
    height: 100%;
    top: 0px;
    position: fixed;
    z-index: 99999;
    background: #fff; }

  .lds-ripple {
    display: inline-block;
    position: relative;
    width: 64px;
    height: 64px;
    position: absolute;
    top: calc(50% - 3.5px);
    left: calc(50% - 3.5px); }
    .lds-ripple .lds-pos {
      position: absolute;
      border: 2px solid #2962FF;
      opacity: 1;
      border-radius: 50%;
      animation: lds-ripple 1s cubic-bezier(0, 0.1, 0.5, 1) infinite; }
    .lds-ripple .lds-pos:nth-child(2) {
      animation-delay: -0.5s; }

  @keyframes lds-ripple {
    0% {
      top: 28px;
      left: 28px;
      width: 0;
      height: 0;
      opacity: 0; }
    5% {
      top: 28px;
      left: 28px;
      width: 0;
      height: 0;
      opacity: 1; }
    100% {
      top: -1px;
      left: -1px;
      width: 58px;
      height: 58px;
      opacity: 0; } }

    /*******************
    Uploade image div
    ********************/
    .preview-images-zone {
        width: 100%;
        border: 1px solid #ddd;
        min-height: 280px;
        /* display: flex; */
        padding: 5px 5px 0px 5px;
        position: relative;
        overflow:auto;
    }
    .preview-images-zone > .preview-image:first-child {
        height: 285px;
        width: 285px;
        position: relative;
        margin-right: 5px;
    }
    .preview-images-zone > .preview-image {
        height: 140px;
        width:  135px;
        position: relative;
        margin-right: 5px;
        float: left;
        margin-bottom: 5px;
    }
    .preview-images-zone > .preview-image > .image-zone {
        width: 100%;
        height: 100%;
    }
    .preview-images-zone > .preview-image > .image-zone > img {
        width: 100%;
        height: 100%;
    }
    .preview-images-zone > .preview-image > .tools-edit-image {
        position: absolute;
        z-index: 100;
        color: #fff;
        bottom: 0;
        width: 100%;
        text-align: center;
        margin-bottom: 10px;
        display: none;
    }
    .preview-images-zone > .preview-image > .image-cancel {
        font-size: 18px;
        position: absolute;
        top: 0;
        right: 0;
        font-weight: bold;
        margin-right: 10px;
        cursor: pointer;
        display: none;
        z-index: 100;
    }
    .preview-image:hover > .image-zone {
        cursor: move;
        opacity: .5;
    }
    .preview-image:hover > .tools-edit-image,
    .preview-image:hover > .image-cancel {
        display: block;
    }
    .ui-sortable-helper {
        width: 90px !important;
        height: 90px !important;
    }
    .var_editimage 
    {
        cursor: pointer;
    }

     #order_div{
        width:100px; 
    }
    #order_div input{
        float: left;
        width:100px; 
    }
    #order_div a{
        float: left;
        width:40px; 
    }
    #order_div {
        position: absolute;
        display:none;
    }

</style>
</head>
<body class="hold-transition sidebar-mini">

  <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
<div class="preloader" style="display: none;">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <!--<li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>-->
    </ul>

    <!-- SEARCH FORM -->
    <!--<form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>-->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <img src="<?= base_url() ?>assets/img/noimage.jpg" class="brand-image img-circle elevation-2">
        </a>
        <div class="dropdown-menu dropdown-menu-right user-dd  ">

          <span class="with-arrow">
              <span class="bg-primary"></span>
          </span>
          <div class="d-flex no-block align-items-center p-15 bg-primary text-white m-b-10">
              <div class="">
                    <img src="<?=base_url()?>assets/img/noimage.jpg" alt="user" class="img-circle" width="60">
              </div>
              <div class="ml-3">
                  <h4 class=""><?=$this->session->firstname?> <?=$this->session->lastname?></h4>
              </div>
          </div>
          <a class="dropdown-item" href="<?= base_url() ?>admin/dashboard/myaccount">
              <i class="fa fa-user mr-1 ml-1"></i> My Profile
          </a>
          
          <a class="dropdown-item" href="<?= base_url() ?>admin/dashboard/changepasswordform">
              <i class="fa fa-lock mr-1 ml-1"></i> Change Password</a>
          
          <div class="dropdown-divider"></div>
          
          <a class="dropdown-item" href="<?= site_url('admin/dashboard/logout') ?>">
              <i class="fa fa-power-off mr-1 ml-1"></i> Logout</a>
          
      </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a target="_blank" href="<?=site_url()?>" class="brand-link">
      <img class="img-responsive" src="<?= base_url() ?>assets/img/inits.png" alt="logo" class="brand-image elevation-3">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url() ?>assets/img/noimage.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="javascript:void(0)" class="d-block"><?=$this->session->firstname?> <?=$this->session->lastname?></a>
          <!--<ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                  <?=$this->session->firstname?> <?=$this->session->lastname?>
                  <i class="right fa fa-angle-left"></i>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="./index2.html" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Dashboard v2</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="./index3.html" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Dashboard v3</p>
                  </a>
                </li>
              </ul>
            </li>
          <ul/>-->
        </div>
      </div>

      <!-- Sidebar Menu -->
      <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in   -->
        <!-- ============================================================== -->
        <?php include_once 'sidebar.php'; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in  -->
        <!-- ============================================================== -->
      <!-- /.sidebar-menu -->

    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

          <!-- Page title -->
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?= $pageheader ?></h1>
          </div>

          <!-- Breadcrumbs -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url('admin')?>">Home</a></li>
              <?php if(isset($breadCrumbs)) echo $breadCrumbs; ?>
            </ol>
          </div><!-- /.col -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-12">
              <?php include_once "alerts.php"; ?>
          </div>
      </div>
      <!-- ============================================================== -->
      <!-- content - style you can find in   -->
      <!-- ============================================================== -->
      <?php include_once $content_file . '.php'; ?>
      <!-- ============================================================== -->
      <!-- End content - style you can find in  -->
      <!-- ============================================================== -->
      
    </section> 
  </div>

  <!-- Image Order div-->
<div id="order_div">
    <input type="number" data-_id=""  name="new_value" class="form-control" min="0" value="" >
    <a  href="javascript:void(0);"  id="order_save" class="btn btn-success btn-sm saveEdit">
        <i class="far fa-save noSaveEdit" aria-hidden="true"></i>
    </a>
    <a href="javascript:void(0);" id="closeorder_div" class="btn btn-danger btn-sm ">
        <i class="fa fa-times" aria-hidden="true"></i>
    </a>
</div>

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="javascript:void(0)"> Trovolink </a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->



<!-- Bootstrap 4 -->
<script src="<?= base_url() ?>assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?= base_url() ?>assets/admin/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url() ?>assets/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= base_url() ?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url() ?>assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url() ?>assets/admin/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>assets/admin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= base_url() ?>assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url() ?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= base_url() ?>assets/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/admin/plugins/fastclick/fastclick.js"></script>

<script src="<?= base_url() ?>assets/js/jquery.fancybox.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/admin/dist/js/adminlte.js"></script>

<script src="<?=base_url(); ?>assets/js/bootstrap-multiselect.js"></script>

 <!--Notification stylesheet include-->
<link rel="stylesheet" href="<?= base_url() ?>assets/jBox/Source/jBox.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/jBox/Source/plugins/Notice/jBox.Notice.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/jBox/Source/themes/NoticeFancy.css">
<script src="<?= base_url() ?>assets/jBox/Source/jBox.js"></script>
<script src="<?= base_url() ?>assets/jBox/Source/plugins/Notice/jBox.Notice.js"></script>

<script src="<?= base_url() ?>assets/js/waitMe.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url() ?>assets/admin/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>assets/admin/dist/js/demo.js"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
  function run_waitMe(el, num, effect){
    text = 'Please wait...';
    fontSize = '';
    switch (num) {
            case 1:
            maxSize = '';
            textPos = 'vertical';
            break;
            case 2:
            text = '';
            maxSize = 30;
            textPos = 'vertical';
            break;
            case 3:
            maxSize = 30;
            textPos = 'horizontal';
            fontSize = '18px';
            break;
    }
    el.waitMe({
            effect: effect,
            text: text,
            bg: 'rgba(255,255,255,0.7)',
            color: '#000',
            maxSize: maxSize,
            waitTime: -1,
            source: 'img.svg',
            textPos: textPos,
            fontSize: fontSize,
            onClose: function(el) {}
        });
    }
    $(document).ready(function() {
      $('.fancybox').fancybox({
        padding   : 0,
        maxWidth  : '100%',
        maxHeight : '100%',
        width   : 560,
        height    : 315,
        autoSize  : true,
        closeClick  : true,
        openEffect  : 'elastic',
        closeEffect : 'elastic'
      });
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#category_list').multiselect({
            
            nonSelectedText: 'Check the Category option!',
            enableClickableOptGroups: true,
            enableCollapsibleOptGroups: true,
            includeSelectAllOption: true,
            allSelectedText: 'All Category Selected ...',
            enableFiltering: true,
            enableFullValueFiltering: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Search for Category...',
            maxHeight: 250,
            buttonWidth: '100%'
        });
        
    });
    
</script>        
</body>
</html>
