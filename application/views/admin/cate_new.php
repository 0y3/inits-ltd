

    <div class=" col-md-12">
                <?php
                    if(isset($cateinfo))
                    { 
                        $action='editcate/'.$cateinfo['id'];
                        $savename='Update';
                    }
                    else{ 
                        $action='savecate';
                        $savename='Save';

                    }
                ?>
        <div class="card card-default">
            <form id="" action="<?= site_url('admin/business/'.$action) ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">

                <div class="col-md-12">
                    <div class="col-md-8 col-md-offset-2 alert alert-danger alert-dismissable get_error" style="display: none;">

                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>

                        <span class="error_msgr_lg"> </span>

                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="save_name">Name</label>
                        <input type="text" id=""  name="name" class="form-control" placeholder="" value="<?php if(isset($cateinfo)) echo $cateinfo['catename']?>" required >
                    </div>

                    <?php if(isset($cateinfo)): ?>
                    <div class="form-group">

                        
                        <div class="col-sm-6">
                            <label>Status</label>
                            <br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="status" value="1" class="custom-control-input" required="" <?php if($cateinfo['status']==1) echo 'checked';?> >
                                <label class="custom-control-label  text-success" for="customRadioInline1">Active</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="status"  required=""  <?php if($cateinfo['status']==0) echo 'checked';?> value="0" class="custom-control-input">
                                <label class="custom-control-label text-danger" for="customRadioInline2">In-Active</label>
                            </div>
                        </div>

                    </div>
                    <?php endif; ?>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-success sbmtbtn"><i class="fa fa-floppy-o"></i> &nbsp; <?= $savename?> &nbsp; </button>
                </div>
            </form>
        </div>
    </div>