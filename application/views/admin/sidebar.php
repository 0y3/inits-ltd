    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?= site_url('admin/dashboard')?>" class="nav-link <?php if(isset($mainmenu) && $mainmenu=="dashboard") echo ' active';?>">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('admin/business')?>" class="nav-link <?php if(isset($mainmenu) && $mainmenu=="business") echo ' active';?>">
              <i class="nav-icon fa fa-upload"></i>
              <p>
                Business 
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('admin/business/categories ')?>" class="nav-link <?php if(isset($mainmenu) && $mainmenu=="categories") echo ' active';?>">
              <i class="nav-icon fa fa-tag"></i>
              <p>
                Categories  
              </p>
            </a>
          </li>

        </ul>
    </nav>