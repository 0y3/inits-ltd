
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="description" content="<?= $meta_keyword ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $titel ?></title>

    

        <!-- Required CSS files -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/barfiller.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/slicknav.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">

    <script src="<?= base_url() ?>assets/js/jquery-2.2.4.min.js"></script>

    <style type="text/css">

        .sm_header
        {
            background-image: url("./assets/img/header-background.jpg");
            height: 100%; 
            background-repeat: no-repeat;
            /*background-size: 100% 100%;*/

            /* Center and scale the image nicely */
            background-position: center;
            -webkit-background-size: cover;
               -moz-background-size: cover;
                 -o-background-size: cover;
                    background-size: cover;
            background-color:unset;
        }
        .Partnerbg
        {
            background-image: url("./assets/img/shadow-line.png");
            height: 100%; 
            background-repeat: no-repeat;
            /*background-size: 100% 100%;*/

            /* Center and scale the image nicely */
            background-position: top;
            -webkit-background-size: top;
               -moz-background-size: top;
                 -o-background-size: top;
                    background-size: top;
        }
        .followbg
        {
            background-image: url("./assets/img/aboutshadow-line.png");
            height: 100%; 
            background-repeat: no-repeat;
            /*background-size: 100% 100%;*/

            /* Center and scale the image nicely */
            background-position: bottom;
            -webkit-background-size: bottom;
               -moz-background-size: bottom;
                 -o-background-size: bottom;
                    background-size: bottom;
        }
        #menu ul > li.current-menu-item > a {
            color: #b5c3d4;
            text-decoration: underline;
            text-decoration-style: solid;
            text-decoration-color: #e67817;
            text-decoration-style: solid; 
        }
        @media only screen and (max-width: 991px){
            .hero-slider .single-slide
            {
                z-index: unset;   
            }
        }
        .sp 
        {
            padding-top: 90px;
            padding-bottom: 60px;
        }
        .colorBg
        {
            background-color: #2d3666;
            border-color: #2d3666 !important;
        }
        .colorB
        {
            color: #2d3666 !important;
        }
        .colorbrg
        {
            background-color: #e67817;
            border-color: #e67817 !important;
            padding-left:40px;
            padding-right: 40px; 
        }
        .colorbr
        {
            color: #e67817 !important;
        }
        .portfolio-img .hover-content 
        {
            background-color: #2f3337 !important;
        }
        .spamh2
        {
            font-weight:bolder;
            font-size: 28px;
        }
        .spamp
        {
            font-size: 20px;
            font-weight: bold;
        }
        .new-slider
        {
            height: 460px !important;
        }


/*------------------------
    6. Modal style
--------------------------*/

.modal-body {
    display: flex;
    justify-content: space-between;
    padding: 20px 30px 50px 30px;
}
.modal-dialog {
    /*margin: 100px auto;*/
    min-width: 650px;
}
@media only screen and (max-width: 767px) {
    .modal-dialog {
        min-width: unset;
    }
    .pd{
        padding-bottom: 10px;
    }
}
.quick-view-tab-content .tab-pane>img {
    width: 100%;
}
.quick-view-list {
    margin-top: 10px;
}
.quick-view-list a {
    margin-right: 10px;
    margin-bottom: 10px;
}
.quick-view-list a:last-child {
    margin-right: 0px;
}
.qwick-view-left {
    flex: 0 0 320px;
    margin-right: 30px;
}
.quick-view-tab-content .tab-pane>img {
    flex: 0 0 320px;
}
.quick-view-list a img {
    flex: 0 0 100px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.10);
}
.modal-content {
    border-radius: 0rem;
}
.qwick-view-content>h3 {
    color: #454545;
    font-size: 20px;
    font-weight: bold;
    margin: 10px 0 5px;
}
.price span {
    color: #707070;
    font-size: 18px;
    font-weight: 400;
}
.price span.new {
    color: #26c6d0;
    margin-right: 12px;
}
.price span.old {
    color: #707070;
    text-decoration: line-through;
}
.quick-view-rating i {
    color: #000000;
    font-size: 18px;
    margin-right: 5px;
}
.quick-view-rating i.red-star {
    color: #26c6d0;
}
.rating-number {
    display: flex;
    justify-content: flex-start;
    margin-bottom: 30px;
}
.quick-view-number>span {
    color: #808080;
    display: block;
    font-size: 14px;
    margin: 3px 0 0 10px;
}
.qwick-view-content > p {
  letter-spacing: 0.2px;
  margin-bottom: 25px;
  color: #454545;
}
.select-option-part label {
    color: #454545;
    font-size: 15px;
    font-weight: bold;
    margin-bottom: 14px;
}
.select-option-part {
    margin-bottom: 25px;
}
.select-option-part select {
    -moz-appearance: none;
    -webkit-appearance: none;
    border: 1px solid #dcdcdc;
    box-shadow: none;
    color: #454545;
    font-size: 14px;
    height: 43px;
    padding-left: 20px;
    position: relative;
    width: 100%;
    background: rgba(0, 0, 0, 0) url("../../assets/img/icon-img/select.png") no-repeat scroll right 20px center;
    cursor: pointer;
}
.qtybutton {
    color: #727272;
    cursor: pointer;
    float: left;
    font-size: 16px;
    font-weight: 600;
    height: 20px;
    line-height: 20px;
    position: relative;
    text-align: center;
    width: 20px;
}
input.cart-plus-minus-box {
    background: transparent none repeat scroll 0 0;
    border: medium none;
    float: left;
    font-size: 16px;
    height: 25px;
    margin: 0;
    padding: 0;
    text-align: center;
    width: 25px;
}
.cart-plus-minus *::-moz-selection {
    background: transparent none repeat scroll 0 0;
    color: #333;
    text-shadow: none;
}
.cart-plus-minus {
    border: 1px solid #dcdcdc;
    overflow: hidden;
    padding: 12px 0 7px 5px;
    width: 80px;
    height: 46px;
}
.quickview-plus-minus {
    display: flex;
    justify-content: flex-start;
    padding-top: 5px;
}
.quickview-btn-cart>a {
    padding: 12px 35px 11px;
}
.quickview-btn-wishlist>a {
    border: 1px solid #dcdcdc;
    color: #454545;
    display: inline-block;
    font-size: 22px;
    padding: 7px 18px 5px;
    z-index: 9;
}
.quickview-btn-wishlist>a:hover {
    border: 1px solid #26c6d0;
    background-color: #26c6d0;
    color: #fff;
}
.quickview-btn-wishlist a {
    overflow: hidden;
}
.quickview-btn-cart {
    margin: 0 30px;
}
.qtybutton.inc {
    margin-top: 2px;
}
.price {
    margin: 9px 0 8px;
}
#modalgivehope .close {
    font-weight: 700;
    line-height: 1;
    opacity: 1;
    text-shadow: 0 1px 0 #fff;
    transition: all .3s ease 0s;
    cursor: pointer;
}
#exampleModal .close:hover,
#exampleCompare .close:hover {
    color: #26c6d0;
}
.modal-backdrop.show {
    opacity: 0.8;
}
.modal-content .close:hover {
    color: #26c6d0;
}
.modal-open .modal {
    z-index: 99999;
}
.modal-backdrop.show {
    z-index: 9999;
}
.quick-view-tab-content .tab-pane {
    border: 1px solid #ddd;
}
.quickview-style-2 .product-rating i.theme-color {
    color: #ff7f00;
}
.quickview-style-2 .btn-style:hover {
    background-color: #ff7f00;
    color: #ffffff;
}
.quickview-style-2 .quickview-btn-wishlist>a:hover {
    background-color: #ff7f00;
    border: 1px solid #ff7f00;
    color: #fff;
}
.compare-style-2 .table-content.compare-style th a.compare-btn:hover,
.compare-style-2 .table-content.compare-style th a:hover,
.compare-style-2#exampleCompare .close:hover,
.quickview-style-2#exampleModal .close:hover {
    color: #ff7f00;
}
.modal-body .cart-plus-minus span {
  display: none;
}

.input-group-prepend {
    margin-right: -1px;
}
.input-group-text {
    display: flex;
    align-items: center;
    padding: .375rem .75rem;
    margin-bottom: 0;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    text-align: center;
    white-space: nowrap;
    background-color: #e9ecef;
    border: 1px solid #ced4da;
    border-radius: .25rem;
}
.input-group-text-front {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.input-group-text-back {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
}
.paypaldiv
{
    position: relative;
}
.paypal
{
    top: 10px;
    left: 150px;
    position: absolute;
}
    </style>
</head>
<body>
    <div class="preloader">
        <span class="preloader-spin"></span>
    </div>
    <div class="site">
        
        <!-- ============================================================== -->
        <!-- header  -->
        <!-- ============================================================== -->
        <?php include_once (dirname(__FILE__).'/../included/header.php'); ?>
        <!-- ============================================================== -->
        <!-- End header - -->
        <!-- ============================================================== -->



        <!-- ============================================================== -->
        <!-- Content  -->
        <!-- ============================================================== -->
        <?php
            if(isset($page_loader))
            {
                if ( file_exists( APPPATH.'views/'.$page_loader.'.php' ) ) 
                {
                    $this->load->view($page_loader); // Sidebar tray
                }
                else 
                {
                    $this->load->view($error_page); //error page
                }

            }
            else
            {
                $this->load->view($error_page); //error page
            }
        ?>
        <!-- ============================================================== -->
        <!-- End Content  -->
        <!-- ============================================================== -->      
        


        <!-- ============================================================== -->
        <!-- Footer  -->
        <!-- ============================================================== -->
        <?php include_once (dirname(__FILE__).'/../included/footer.php'); ?>
        <!-- ============================================================== -->
        <!-- End Footer - -->
        <!-- ============================================================== -->
       
       <!-- modal -->
        <div class="modal fade" id="modalgivehope" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header mod-head" >
                        <h4 class="modal-title text-left">Give Hope</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-close" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="modal-body row">
                        <div class="qwick-view-leftt col-md-12">
                            <div class="quick-view-learg-img">
                                <div class="quick-view-tab-content tab-content">
                                    <div class="tab-pane active show fade" id="modal1" role="tabpanel">
                                        <img src="<?= site_url('assets/img/contact-slide.png') ?>" alt="">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="qwick-view-rightt col-md-12">
                            <div class="qwick-view-content">
                                <h3 style="color:#17a2b8 ">Give Hope</h3>
                                <!--<div class="product-price">
                                    <span class="old">$19.00 </span>
                                    <span class="new">$17.00</span>
                                </div>-->
                                <p><b>Hope Emerge is funded entirely by public contributions. Your donations to our 501(c)(3) Charitable foundation will help maintain and expand our course of giving a meaningful life to others. Know that your gift will impact the lives of many. For that we thank you!</b></p>
                                <div class="quick-view-select">
                                    <div class="select-option-part">
                                        <label >Donate*</label>
<div class="row">
    <div class="col-md-3 pd">
        <form action="https://secure.networkmerchants.com/cart/cart.php" method="POST">
            <input type="hidden" name="key_id" value="13207641" />
            <input type="hidden" name="action" value="process_fixed" />
            <input type="hidden" name="amount" value="20.00" />
            <input type="hidden" name="order_description" value="Give Hope ($20)" />
            <input type="hidden" name="language" value="en" />
            <input type="hidden" name="url_finish" value="https://hopeemerge.org/givehope/paymentsuccessful" />
            <input type="hidden" name="url_cancel" value="https://hopeemerge.org/givehope/cancel" />
            <input type="hidden" name="customer_receipt" value="true" />
            <input type="hidden" name="merchant_receipt_email" value="hopeemerge@gmail.com,trivin98@gmail.com" />
            <input type="hidden" name="hash" value="action|amount|order_description|8310bb18884f86ccf6691172a3f7c923" />
            <button type="submit"  name="submit" value="Donate" class="btn btn-info btn-flat paysbmtbtn">$20.00 (USD)</button>
        </form>
    </div>

    <div class="col-md-3 pd"> 
        <form action="https://secure.networkmerchants.com/cart/cart.php" method="POST">
            <input type="hidden" name="key_id" value="13207641" />
            <input type="hidden" name="action" value="process_fixed" />
            <input type="hidden" name="amount" value="50.00" />
            <input type="hidden" name="order_description" value="Give Hope ($50.00)" />
            <input type="hidden" name="language" value="en" />
            <input type="hidden" name="url_finish" value="https://hopeemerge.org/givehope/paymentsuccessful" />
            <input type="hidden" name="url_cancel" value="https://hopeemerge.org/givehope/cancel" />
            <input type="hidden" name="customer_receipt" value="true" />
            <input type="hidden" name="merchant_receipt_email" value="hopeemerge@gmail.com,trivin98@gmail.com" />
            <input type="hidden" name="hash" value="action|amount|order_description|86f656b71414946761d1185b007d8820" />
            <button type="submit"  name="submit" value="Donate" class="btn btn-info btn-flat paysbmtbtn">$50.00 (USD)</button>
        </form>    
    </div>

    <div class="col-md-3 pd">
        <form action="https://secure.networkmerchants.com/cart/cart.php" method="POST">
            <input type="hidden" name="key_id" value="13207641" />
            <input type="hidden" name="action" value="process_fixed" />
            <input type="hidden" name="amount" value="100.00" />
            <input type="hidden" name="order_description" value="Give Hope ($100.00)" />
            <input type="hidden" name="language" value="en" />
            <input type="hidden" name="url_finish" value="https://hopeemerge.org/givehope/paymentsuccessful" />
            <input type="hidden" name="url_cancel" value="https://hopeemerge.org/givehope/cancel" />
            <input type="hidden" name="customer_receipt" value="true" />
            <input type="hidden" name="merchant_receipt_email" value="hopeemerge@gmail.com,trivin98@gmail.com" />
            <input type="hidden" name="hash" value="action|amount|order_description|6bdf403faca97c749e87e0c82a16250d" />
            <button type="submit"  name="submit" value="Donate" class="btn btn-info btn-flat paysbmtbtn">$100.00 (USD)</button>
        </form> 
    </div>
</div>
<div class="row" style="padding-top: 10px;">
    <div class="col-md-9">
        <form action="https://secure.networkmerchants.com/cart/cart.php" method="POST">
            <input type="hidden" name="allow_recurring" value="true">
            <input type="hidden" name="key_id" value="13207641" />
            <input type="hidden" name="action" value="process_variable" />
            <input type="hidden" name="order_description" value="Give Hope(Donation)" />
            <input type="hidden" name="language" value="en" />
            <input type="hidden" name="url_finish" value="https://hopeemerge.org/givehope/paymentsuccessful" />
            <input type="hidden" name="url_cancel" value="https://hopeemerge.org/givehope/cancel" />
            <input type="hidden" name="customer_receipt" value="true" />
            <input type="hidden" name="merchant_receipt_email" value="hopeemerge@gmail.com,trivin98@gmail.com" />
            <input type="hidden" name="hash" value="action|order_description|ca4971a8a4a3a244771d7145781eb54a" />
            <input type="hidden" name="return_link" value="https://hopeemerge.org/givehope/paymentsuccessful" />
            <input type="hidden" name="return_method" value="redirect" />
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text input-group-text-front">$</span>
                </div>
                
                <input type="number" name="amount" min="0.00" placeholder="Donation Amount" class="form-control donate">

                <div class="input-group-append">
                    <span class="input-group-text input-group-text-back">.00</span>
                </div>
                <span class="input-group-append">
                    <button type="submit"  name="submit" value="Donate" class="btn btn-info btn-flat paysbmtbtn">Other Amount</button>
                </span>
            </div>
        </form>
    </div>
</div>
                                    </div>
                                    <h3>OR</h3>
                                    <div class="select-option-part paypaldiv">
                                        <label><img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_150x38.png"></label>
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="paypal">
                                            <input type="hidden" name="cmd" value="_s-xclick" />
                                            <input type="hidden" name="hosted_button_id" value="LZTRKZN4YSDFW" />
                                            <input type="image" class="paysbmtbtn" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
                                            <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
                                        </form>    
                                    </div>
                                    <p>All funds will be converted to USD (see your bank for conversion rate)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
</div>

    <!--Required JS files-->
<script src="<?= base_url() ?>assets/js/vendor/popper.min.js"></script>
<script src="<?= base_url() ?>assets/js/vendor/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/vendor/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>assets/js/vendor/isotope.pkgd.min.js"></script>
<script src="<?= base_url() ?>assets/js/vendor/jquery.barfiller.js"></script>
<script src="<?= base_url() ?>assets/js/vendor/loopcounter.js"></script>
<script src="<?= base_url() ?>assets/js/vendor/slicknav.min.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.fancybox.min.js"></script>
<script src="<?= base_url() ?>assets/js/active.js"></script>

<script>
    $(document).on("click",".paysbmtbtn", function(e){
        $('#modalgivehope').modal('hide');
        $('.preloader').css("display", "block");
    });

</script>

</body>

</html>
