



       
        

        <!-- ============================================================== -->
        <!-- Our Service  -->
        <!-- ============================================================== -->
        <div class="service-area bg2 sp">
            <div class="container">
                <div class="search-wrapper">
                    <form action="<?= site_url('')?>" method="get">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="row">
                                    <div class="col-lg-10 ">
                                        <input type="text" class="form-control" name="biznamelike" placeholder="Search for Business name">
                                    </div>
                                    <div class="col-lg-2">
                                        <button class="btn btn-info btn-join">search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>  

            <div class="container">
                <div class="row">
<?php if(!empty($biz)): ?>
    <?php $count=1; ?>
    <?php foreach ($biz as $bizs) :?>
                    <div class="col-lg-4 col-md-6 single-service single-portfolio">
                        <div class="inner">
                            <div class="portfolio-img">
                                <img src="<?= site_url()?>assets/img/inits.png" alt="">
                                <div class="hover-content">
                                    <div>
                                        <a href="<?= site_url('business/details/'.$bizs['slug'])?>"  class="button">Read More...</a>
                                    </div>
                                </div>
                            </div>
                            <div class="title portfolio-content">
                                <a href="<?= site_url('business/details/'.$bizs['slug'])?>"><h3 title="<?=$bizs['bizname']?>">
                                    <?php
                                            $value = $bizs['bizname'];
                                                $limit = '20';
                                                if (strlen($value) > $limit) {
                                                         $trimValues = substr($value, 0, $limit).'...';
                                                          } 
                                                else {
                                                        $trimValues = $value;
                                                  }
                                            //character_limiter($resta['companydesc'],25); 
                                                  echo $trimValues;
                                    ?> </h3></a>
                            </div>
                            <div class="content">
                                <p title="<?=$bizs['description']?>">
                                        <?php
                                            $value = $bizs['description'];
                                                $limit = '100';
                                                if (strlen($value) > $limit) {
                                                         $trimValues = substr($value, 0, $limit).'...';
                                                          } 
                                                else {
                                                        $trimValues = $value;
                                                  }
                                            //character_limiter($resta['companydesc'],25); 
                                                  echo $trimValues;
                                    ?> 
                                    <a href="<?= site_url('business/details/'.$bizs['slug'])?>">Read More...</a>
                                </p>
                            </div>
                                <p style="margin-bottom:unset;">Phone: <b><?=$bizs['phone']?></b></p>
                                <p style="margin-bottom:unset;">Email: <b><?=$bizs['email']?></b></p>
                                <p style="margin-bottom:unset;">website: <b><?=$bizs['website']?></b></p>
                                <p style="margin-bottom:unset;">Categories: 
                                    <?php foreach ($bizs['biz_cate'] as $bizcate) :?>
                                        <span id="<?=$bizcate['id'] ?>" class="cat_span mr-2"><?=$bizcate['catename']?> </span>
                                      <?php endforeach; ?>
                                  </p>
                        </div>
                    </div>
    <?php $count++; ?>
    <?php endforeach; ?>

<?php else: ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 mb-20">
                            <h1>No Record </h1>
                        </div>
<?php endif; ?>
                    
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Our Service  -->
        <!-- ============================================================== -->


       


        

        <!-- ============================================================== -->
        <!-- Partner  -->
        <!-- ============================================================== -->
        <div class="brand-area Partnerbg ">
            <div class="container">
                <div class="section-title">
                    <h2 class="colorB">Our Partner & Sponsors</h2>
                </div>
                <div class="partner-slider">
                    <div class="single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo1.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo2.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo4.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo6.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo7.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo8.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo9.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class=" single-brand">
                        <div class="inner">
                            <a href="#">
                                <img src="<?= site_url()?>assets/img/ourpartner-logo10.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Partner  -->
        <!-- ============================================================== -->


       <!-- Active -->
       <script type="text/javascript">
           $(document).ready(function(){
                $('.current-menu-item').removeClass('current-menu-item').addClass('sitenav');
                $('.home').removeClass('sitenav').addClass('current-menu-item');
           });
       </script>
       
        
