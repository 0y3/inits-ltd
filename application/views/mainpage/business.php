
        <!-- ============================================================== -->
        <!-- Contact  -->
        <!-- ============================================================== -->
        <div class="contact-area">
            <div class="container">
<?php foreach ($biz as $bizs) :?>
              <div class="row">
                  <div class="col-12">
                      <?php $this->load->view('admin/alerts');// include_once "/alerts.php"; ?>
                  </div>
              </div>

                <div class="row">
                    <div class="col-md-10 mb-3 contact-info">
                        <div class="single-info">
                            <h2><?=$bizs['bizname']?></h2>
                        </div>
                        <div class="single-info">
                            <h5>Description</h5>
                            <p><?=$bizs['description']?></p>
                        </div>
                    </div>
                    <div class="col-md-5 contact-info">
                        <div class="single-info">
                            <h5>Categories:</h5>
                            <p>
                                <?php foreach ($bizs['biz_cate'] as $bizcate) :?>
                                    <span id="<?=$bizcate['id'] ?>" class="cat_span mr-2"><i class="fa fa-caret-right "></i> <?=$bizcate['catename']?> </span>
                                <?php endforeach; ?>
                            </p>
                        </div>
                        <div class="single-info">
                            <h5>Phone</h5>
                            <p><b><?=$bizs['phone']?></b></p>
                        </div>
                        <div class="single-info">
                            <h5>Email</h5>
                            <p><b><?=$bizs['email']?></b></p>
                        </div>
                        <div class="single-info">
                            <h5>Address</h5>
                            <b><p><?=$bizs['address']?></p></b>
                        </div>
                        <div class="single-info">
                            <h5>Website</h5>
                            <p><a target="_blank" href="<?=$bizs['website']?>"><b><?=$bizs['website']?></b></a></p>
                        </div>
                        
                    </div>
                </div>
<?php endforeach; ?>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Contact  -->
        <!-- ============================================================== -->




        <div class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-12" style="padding: 30px 0;">
                        <img src="<?= site_url()?>assets/img/contact-stripe.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        

       <!-- Active -->
       <script type="text/javascript">
           $(document).ready(function(){
                $('.current-menu-item').removeClass('current-menu-item').addClass('sitenav');
                $('.contact').removeClass('sitenav').addClass('current-menu-item');
           });
       </script>
       
        
