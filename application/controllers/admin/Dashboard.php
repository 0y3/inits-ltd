<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Dashboard (DashboardController)
 * Dashboard Class to control all main  related operations.
 * @author : Oye Segun:-- Novo3 Tecch
 * @version : 1.1
 * @since : 15 March 2019
 */
class Dashboard extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->helper('text');
        $this->load->helper('string');
        $this->isLogIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        
        $data['icon'] = 'icon.ico';
        $data ['meta_keyword']= 'Creative,NGO,Nigerial NGO,Dashboard';
        $data['error_page'] = 'admin/access';
        $data['titel'] = 'Hope Emerge:- Dashboard';
        $data['pageheader'] = 'Dashboard';
        $data['mainmenu'] = "dashboard";
        //$data['breadCrumbs'] = '<li class="breadcrumb-item active">Dashboard</li> ';
        $countproject= $this->Generic->getCountByField(null,null , $tablename='tbl_biz');
        $countuser= $this->Generic->getCountByField(null,null , $tablename='tbl_cate');

        $data['countproject']=$countproject ;
        $data['countuser']=$countuser ;

        $data ['content_file']= 'dashboard';
        $this->load->view('admin/layout', $data);
    }

    function validateadmin()
    {
        if (! isset ( $_SESSION['isLogIn'] ) || $_SESSION['isLogIn'] != TRUE) 
        {
           redirect('admin');
        }
    }

    /**
     * This function is used to logged out user from system
     */
    function logout() {
        $this->session->sess_destroy ();
        
        redirect ( 'admin' );
    }

    public function myaccount()
    {   
        $this->validateadmin();
        $data ['meta_keyword']= 'Creative,NGO,Nigerial NGO,Dashboard';
        $data['mainmenu'] = "dashboard";
        
        $data['titel'] = 'Hope Emerge:- Users Details';
        $userinfo = $this->Generic->getByFieldSingle('id', $_SESSION['userId'], $tablename='tbl_adminusers');
        if($userinfo==false)
        {
            redirect('admin');
        }
        $data['userinfo']=$userinfo ;
        //print("<pre>".print_r($this->Generic->getByFieldSingle('id', $id, $tablename='admin_users'),true)."</pre>");die;
        $data ['content_file']= 'account';
        $data['pageheader'] = "Edit User Form";
        $data['breadCrumbs'] = '<li class="breadcrumb-item active">User Details</li>';
        
        $this->load->view('admin/layout', $data);
        
    }
    // Controller function to edit a specified user
    public function editaccount($id)
    {
        $data_New = array(  
                        'firstname'     => $this->input->post('firstname'),
                        'lastname'      => $this->input->post('lastname'),
                        'phone'         => $this->input->post('phone')
                     );
        
        $data_Where = array(  
                        'id'    => $id
                     );

        //$insert_data = $this->user->_updateuser($data_New);
        // insert to db
        $insert_data = $this->Generic->editByConditions($data_New, $data_Where , $tablename="tbl_adminusers"); 

        //print("<pre>".print_r($insert_data,true)."</pre>");die;
        if($insert_data)
            {
                $this->session->set_flashdata('success','User Info Updated');
                $this->session->set_flashdata('message', 'User Info Updated');
                $Json_resultSave = array ( 'status' => '1');
                echo json_encode($Json_resultSave);
                exit();

            }
        else {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Updated User Info');
            $Json_resultSave = array ( 'status' => '0');
            echo json_encode($Json_resultSave);
            exit();
        }
    }
    

    public function changepasswordform()
    {   
        $this->validateadmin();
        $data ['meta_keyword']= 'Creative,NGO,Nigerial NGO,Dashboard';
        $data['mainmenu'] = "dashboard";
        
        $data['titel'] = 'Hope Emerge:- Users Change Password Form';
        $data ['content_file']= 'passwordform';
        $data['pageheader'] = " Change Password Form";
        $data['breadCrumbs'] = '<li class="breadcrumb-item active">Change Password Form</li>';
        
        $this->load->view('admin/layout', $data);
        
    }


    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        
        $newPassword = $_POST["cfmpwd"];   
        $oldPassword = $_POST["oldpwd"];
        $userinfo = $this->Generic->getByFieldSingle('id', $_SESSION['userId'], $tablename='tbl_adminusers');

        //print("<pre>".print_r(verifyHashedPassword($oldPassword, $userinfo['password']),true)."</pre>");die;
        if(verifyHashedPassword($oldPassword, $userinfo['password']))
        {
            
            $data_New=array('password' =>  getHashedPassword($newPassword) );
            
            $this->Generic->edit($data_New, (int)$this->session->userId, $tablename="tbl_adminusers"); 
            
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Password Change Successfull...');
            $Json_resultSave = array (
                                    'status' => '1',
                                    'content' => 'Password Change .....'
                                    );
            echo json_encode($Json_resultSave);
            exit();
            
        }
        else 
         {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'Old Password Incorrect Password....');
            $Json_resultSave = array (
                                    'status' => '0',
                                    'content' => 'Incorrect Password.....'
                                    );
            echo json_encode($Json_resultSave);
            exit();
         }

    }


    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $name = ucwords(strtolower($this->input->post('fname')));
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->input->post('mobile');
                
                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,
                                    'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('addNew');
            }
        }
    }

   
}

?>