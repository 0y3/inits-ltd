<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Authentication (LoginController)
 * Authentication class to control to authenticate user credentials and starts user's session.
 * @author : Oye Segun:-- Novo3 Tecch
 * @version : 1.1
 * @since : 15 March 2019
 */
class Authentication extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Utility');
        $this->load->model('Generic');
        $this->load->helper('text');
        $this->load->helper('string');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->login();
    }
    
    /**
     * This function used to check the user is logged in or not
     */
    function login()
    {
        $isLoggedIn = $this->session->userdata('isLogIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE )
        {
            $data['icon'] = 'icon.ico';
            $data ['meta_keyword']= 'Creative,INITS,Nigerial,Business Directory Service';
            $data['titel'] = 'INITS Business Directory Service:- Admin Login';
            $this->load->view('admin/login',$data);
        }
        else
        {
            redirect('admin/dashboard');
        }
    }

    /**
     * This function is used to logged out user from system
     */
    function logout() {
        $this->session->sess_destroy ();
        
        redirect ( 'admin' );
    }

    
    
    /**
     * This function used to logged in user
     */
    public function loginMe()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $result  = $this->Generic->getByFieldSingle('email',$email, $tablename="tbl_adminusers");// get from db
            
            if($result)
            {   
                //$getrole  = $this->Generic->getByFieldSingle('id',$result['userroleid'], $tablename="tbl_role");// get role name

                if($result['status']==0)
                {
                    $this->session->set_flashdata('error', 'Email Disactivate!! Contact Admin');
                    redirect('admin/authentication/login');
                }
                if(verifyHashedPassword($password, $result['password']))
                {
                    $sessionArray = array(
                                            'userId'=>$result['id'],                    
                                            //'role'=>$result['userroleid'],
                                            //'roleText'=>$getrole['rolename'],
                                            'firstname'=>$result['firstname'],
                                            'lastname'=>$result['lastname'],
                                            'isLogIn' => TRUE
                                    );
                                    
                    $this->session->set_userdata($sessionArray);
                    redirect('admin/dashboard');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Email or password mismatch');
                    redirect('admin/authentication/login');
                }
                
            }
            else
            {
                $this->session->set_flashdata('error', 'Email Not Found');
                redirect('admin/authentication/login');
            }
        }
    }

    


}

?>