<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Businesses');
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->helper('text');
    }
    
    public function index($existing_search=0, $page=0)
    {
        //print_r($_SESSION);
        $this->validateadmin();
            
       
        $data['error_page'] = 'admin/access';
        $data ['meta_keyword']= 'Creative,INITS,Nigerial,Business Directory Service';
        $data['titel'] = 'INITS Business Directory Service:- Business List';

        $data['pageheader'] = "Business List";
        $data['breadCrumbs'] = '<li class="breadcrumb-item active">Business lists</li>';
        $data['mainmenu'] = "business";
        
        $filterparams = array();
        $query_params = array();
        // Process filter if any was posted
            
        if(($this->input->post() && $existing_search == 0)|| ($this->input->get() && $existing_search == 0))
        {
            // Get all data from the search form

            $filterparams = $this->input->get();
            $this->session->set_userdata(array('search_params' => $filterparams));
            $existing_search = 1;
        }
        else if($existing_search == 1)
        {
            // if there is an existing search it means the parameters are already in the session
            $filterparams = $this->session->search_order_params;
        }
        
        // sanitize params and only pass along the ones with data
        foreach ($filterparams as $key => $value)
        {
            if ($value != '' && $value != NULL && $value != 'all')
                $query_params[$key] = $value;
        }
        
        // Set back any parameter so the filter forms can have the data searched
        $data['filterparams'] = $filterparams;

        // Load all the Biz
        $biz = $this->Businesses->getAll($query_params, $page);
        $data['biz'] = $biz;
        $all_count =  $this->Businesses->getAllCount($query_params);
        $data['counts']=$all_count;
        

        //print("<pre>".print_r( $filterparams,true)."</pre>");die;
        //load pagenation details
        $this->load->library('pagination');
        $config['base_url'] = site_url("admin/business/$existing_search");
        $config['total_rows'] = $all_count;
        $config['per_page'] = '25';
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        $data['pagenation'] = $this->pagination->create_links();
        
        //print("<pre>".print_r($biz,true)."</pre>");die;

        $data ['content_file']= 'business-list';
        $this->load->view('admin/layout', $data);
        
    }
    
    // Validate a admin user login
    function validateadmin()
    {
        if (! isset ( $_SESSION['isLogIn'] ) || $_SESSION['isLogIn'] != TRUE) 
        {
           redirect('admin');
        }
    }

    // Controller function to display the add user form
    public function addform($slug=null)
    {
        $this->validateadmin();
        $data ['meta_keyword']= 'Creative,NGO,Nigerial NGO';
        $data['titel'] = 'INITS Business Directory Service:- New Business';
        $data['pageheader'] = "New Business";
        $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/business").'">Business List</a></li> <li class="breadcrumb-item active">New Business</li>';
        $data['mainmenu'] = "business";

        $data['cate'] =$this->Generic->getAll($tablename='tbl_cate', $limit=NULL, $fieldlist=null, $createdat=null, $updatedat=null, $orderbyfield='catename');



        $data ['content_file']= 'business_new';
        $this->load->view('admin/layout', $data);
    }

    // Controller function to display the edit user form
    public function editform($slug=null)
    {
        $this->validateadmin();
        if(!isset($slug)|| empty($slug))
        {
            redirect('admin/business');
        }
        $biz_data  = $this->Generic->getByFieldSingle('slug',$slug, $tablename="tbl_biz");// get from db
        if($biz_data)
        {
            $data['title_type']= 'Edit Business Form';
            $data['bizinfo'] = $biz_data;
            $data['cate'] = $this->Businesses->getAllCateByBiz(null, $biz_data['id']);
            //$data['cate'] =$this->Generic->getAll($tablename='tbl_cate', $limit=NULL, $fieldlist=null, $createdat=null, $updatedat=null, $orderbyfield='catename');

            $data ['meta_keyword']= 'Creative,NGO,Nigerial NGO';
            $data['titel'] = 'INITS Business Directory Service:- Edit Business';
            $data['pageheader'] = "Edit Business Form";
            $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/business").'">Business List</a></li> <li class="breadcrumb-item active">Edit Business</li>';
            $data['mainmenu'] = "business";

            //print("<pre>".print_r($this->Businesses->getAllCateByBiz(null, $biz_data['id']),true)."</pre>");die;
            $data ['content_file']= 'business_new';
            $this->load->view('admin/layout', $data);
        }
        else
        {
            redirect('admin/business');
        }
        
        
    }

    public function save()
    {
        $data_check = array( 
                        'bizname' => $this->input->post('name')
                         );
        $check_name =  $this->Generic->findByCondition($data_check,'tbl_biz');  // call City by State
        if($check_name)
        {
            $this->session->set_flashdata('warning','warning');
            $this->session->set_flashdata('message', '<h1><i class="fa fa-warning"></i>Warning!!!</h1> Business Already Existed... Try Another Name');
            $Json_resultSave = array ('status' => '0' );
            redirect('admin/business/addform');
        }

            $data_New = array(  
                        'bizname'    => $this->input->post('name'),
                        'description'    => $this->input->post('description'),
                        'address'    => $this->input->post('address'),
                        'email'    => $this->input->post('email'),
                        'phone'    => $this->input->post('phone'),
                        'website'    => $this->input->post('website')
                        );
            // slug library
            $config = array(
                    'table' => 'tbl_biz',
                    'id' => 'id',
                    'field' => 'slug',
                    'title' => 'title',
                    'replacement' => 'dash' // Either dash or underscore
                    );
            $this->load->library('slug', $config);

            $data_slug = array(
                    'slug' => $this->input->post("name"),       // slug name of filed in db were ur url is stored
                    );
            $data_New['slug'] = $this->slug->create_uri($data_slug);
            //print("<pre>".print_r($data_New,true)."</pre>");die;
            // insert to db
            $insert_data = $this->Generic->add($data_New, $tablename="tbl_biz"); 
            if($insert_data)
            {
                //save categories
                foreach($_POST['category'] as $key=>$cateid) 
                {
                    $data_cate= array(  
                            'cateid' =>  $cateid,
                            'bizid'  =>  $insert_data
                                     );
                    // insert to Product category db 
                    $insert_cate =$this->Generic->add($data_cate, $tablename='tbl_catebiz');
                }
            }
            if($insert_data)
            {
                $bizinfo=$this->Generic->getByFieldSingle('id', $insert_data, $tablename='tbl_biz');
                $this->session->set_flashdata('success','success');
                $this->session->set_flashdata('message', 'New Business Added');
                $Json_resultSave = array ('status' => '1' );
               // echo json_encode($Json_resultSave);
                redirect('admin/business');

            }
            else 
            {
                $this->session->set_flashdata('error','error');
                $this->session->set_flashdata('message', 'An error occur when Adding New Business');
                $Json_resultSave = array ('status' => '0' );
                    //echo json_encode($Json_resultSave);
                redirect('admin/business/addform');
            }
    }

    // Controller function to edit a specified user
    public function edit($id)
    {   
        $bizinfo=$this->Generic->getByFieldSingle('id', $id, $tablename='tbl_biz');

        $data_check = array(  
                        'bizname' => $this->input->post('name'),
                        'id !='    => $id
                        );
        $checkinfo = $this->Generic->findByCondition($data_check, $tablename='tbl_biz');
        //print("<pre>".print_r($checkinfo,true)."</pre>");die;
        if($checkinfo)
        {
            $this->session->set_flashdata('warning','warning');
            $this->session->set_flashdata('message', '<i class="fa fa-warning"></i> Warning!!!</h1> Business Name Already Exists!!! Try Another Name');
            redirect('admin/business/editform/'.$bizinfo['slug']); 
        }

         $data_New = array(  
                        'bizname'    => $this->input->post('name'),
                        'description'    => $this->input->post('description'),
                        'address'    => $this->input->post('address'),
                        'email'    => $this->input->post('email'),
                        'phone'    => $this->input->post('phone'),
                        'website'    => $this->input->post('website'),
                        'status'        => $this->input->post('status')
                     );
        
        $data_Where = array(  
                        'id'    => $id
                     );
        // slug library
        $config = array(
                'table' => 'tbl_biz',
                'id' => 'id',
                'field' => 'slug',
                'title' => 'title',
                'replacement' => 'dash' // Either dash or underscore
                );
        $this->load->library('slug', $config);

        $data_slug = array(
                'slug' => $this->input->post("name"),       // slug name of filed in db were ur url is stored
                );
        $data_New['slug'] = $this->slug->create_uri($data_slug, $id );

        // insert to db
        $insert_data = $this->Generic->editByConditions($data_New, $data_Where , $tablename="tbl_biz"); 
        if($insert_data)
        {

            $countcatebiz = $this->Generic->getCountByField('bizid',$id, $tablename='tbl_catebiz');
            $id_cate = $this->input->post('category[]');
            $id_count = count($id_cate);

            if(($countcatebiz > $id_count) || ($countcatebiz < $id_count) )
            {

                $this->db->where('bizid', $id);
                $query_delete = $this->db->delete('tbl_catebiz'); // delete all recode first then save a new one.
                if($query_delete)
                {

                    foreach($_POST['category'] as $key=>$cateid) 
                    {
                        $data_cate= array(  
                                'cateid' =>  $cateid,
                                'bizid'  =>  $id
                                         );
                        // insert to Product category db 
                        $insert_cate =$this->Generic->add($data_cate, $tablename='tbl_catebiz');
                    }
                }
            }
        }

        if($insert_data)
            {
                $this->session->set_flashdata('success','Business Info Updated');
                $this->session->set_flashdata('message', 'Business Info Updated');
                $Json_resultSave = array ('status' => '1');
                //echo json_encode($Json_resultSave); exit();
                redirect('admin/business');
            }
        else {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Updated Business Info');
            $Json_resultSave = array ('status' => '0');
            //echo json_encode($Json_resultSave); exit();
            redirect('admin/business/editform/'.$bizinfo['slug']);
        }
        
    }

    

    // Controller function to delete a specified user
    public function delete()
    {
        $by_id = $_POST["_id"];
        //$_data = $this->user->_deleteuser($by_id);

        // delete to db
        $_data =$this->Generic->delete($by_id, $tablename="tbl_biz");
        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Business Deleted');
            $Json_resultSave = array ('status' => '1');
            echo json_encode($Json_resultSave);
            exit();
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting Business');
            $Json_resultSave = array ('status' => '0');
            echo json_encode($Json_resultSave);
            exit();
        }
    }

    
    public function categories($existing_search=0, $page=0)
    {
        //print_r($_SESSION);
        $this->validateadmin();
            
        $data ['meta_keyword']= 'Creative,INITS,Nigerial,Business Directory Service';
        $data['titel'] = 'INITS Business Directory Service:- Business Category List';

        $data['pageheader'] = "Business Category List";
        $data['breadCrumbs'] = '<li class="breadcrumb-item active">Business Category lists</li>';
        $data['mainmenu'] = "categories";
        
        $filterparams = array();
        $query_params = array();
        // Process filter if any was posted
            
        if(($this->input->post() && $existing_search == 0)|| ($this->input->get() && $existing_search == 0))
        {
            // Get all data from the search form

            $filterparams = $this->input->get();
            $this->session->set_userdata(array('search_params' => $filterparams));
            $existing_search = 1;
        }
        else if($existing_search == 1)
        {
            // if there is an existing search it means the parameters are already in the session
            $filterparams = $this->session->search_order_params;
        }
        
        // sanitize params and only pass along the ones with data
        foreach ($filterparams as $key => $value)
        {
            if ($value != '' && $value != NULL && $value != 'all')
                $query_params[$key] = $value;
        }
        
        // Set back any parameter so the filter forms can have the data searched
        $data['filterparams'] = $filterparams;

        // Load all the Biz
        $cate = $this->Businesses->getAllCate($query_params, $page);
        $data['cate'] = $cate;
        $all_count =  $this->Businesses->getAllCateCount($query_params);
        $data['counts']=$all_count;
        

        //print("<pre>".print_r( $filterparams,true)."</pre>");die;
        //load pagenation details
        $this->load->library('pagination');
        $config['base_url'] = site_url("admin/business/categories/$existing_search");
        $config['total_rows'] = $all_count;
        $config['per_page'] = '25';
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        $data['pagenation'] = $this->pagination->create_links();
        
        //print("<pre>".print_r($maintenance,true)."</pre>");die;

        $data ['content_file']= 'cate-list';
        $this->load->view('admin/layout', $data);
        
    }

    public function addformcate($id=null)
    {   
        $this->validateadmin();

        $data ['meta_keyword']= 'Creative,INITS,Nigerial,Business Directory Service';

        $data['mainmenu'] = "categories";
        
        if(isset($id) && !empty($id))
        {

            $data['titel'] = 'INITS Business Directory Service:- Edit Business Category';
            $cateinfo = $this->Generic->getByFieldSingle('id', $id, $tablename='tbl_cate');
            if($cateinfo==false)
            {
                redirect('admin/business/categories');
            }
            $data['cateinfo']=$cateinfo ;
            $data['pageheader'] = "Edit Buisness Categories Form";
            $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/business/categories").'">Business Category lists</a></li> <li class="breadcrumb-item active">Edit Business Category</li>';
        }
        else
        {
            
            $data['titel'] = 'INITS Business Directory Service:- New Business Category List';
            $data['pageheader'] = "Add Business Category";
            $data['breadCrumbs'] = '<li class="breadcrumb-item"><a href="'.site_url("admin/business/categories").'">Business Category lists</a></li> <li class="breadcrumb-item active">Add Business Category</li>';
        }

        $data ['content_file']= 'cate_new';
        $this->load->view('admin/layout', $data);
    }

    public function savecate()
    {
        $data_check = array( 
                        'catename' => $this->input->post('name')
                         );
        $check_name =  $this->Generic->findByCondition($data_check,'tbl_cate');  // call City by State
        if($check_name)
        {
            $this->session->set_flashdata('warning','warning');
            $this->session->set_flashdata('message', '<h1><i class="fa fa-warning"></i>Warning!!!</h1> Business Category Name Already Existed... Try Another Name');
            $Json_resultSave = array ('status' => '0' );
            redirect('admin/business/addformcate');
        }

            $data_New = array(  
                        'catename'    => $this->input->post('name')
                        );
            
            // insert to db
            $insert_data = $this->Generic->add($data_New, $tablename="tbl_cate"); 
            if($insert_data)
            {
                $bizinfo=$this->Generic->getByFieldSingle('id', $insert_data, $tablename='tbl_cate');
                $this->session->set_flashdata('success','success');
                $this->session->set_flashdata('message', 'New Business Category Added');
                $Json_resultSave = array ('status' => '1' );
               // echo json_encode($Json_resultSave);
                redirect('admin/business/categories');

            }
            else 
            {
                $this->session->set_flashdata('error','error');
                $this->session->set_flashdata('message', 'An error occur when Adding New Business Category');
                $Json_resultSave = array ('status' => '0' );
                    //echo json_encode($Json_resultSave);
                redirect('admin/business/addformcate');
            }
    }

    public function editcate($id)
    {
        if(empty($this->input->post('name'))){ redirect('admin/business/editcate/'.$id); }
        $data_check = array(  
                        'catename' => $this->input->post('name'),
                        'id !='    => $id
                        );
        $checkinfo = $this->Generic->findByCondition($data_check, $tablename='tbl_cate');
        //print("<pre>".print_r($checkinfo,true)."</pre>");die;
        if($checkinfo)
        {
            $this->session->set_flashdata('warning','warning');
            $this->session->set_flashdata('message', '<i class="fa fa-warning"></i> Warning!!!</h1> Business Category Name Already Exists!!! Try Another Name');
            redirect('admin/business/addformcate/'.$id); 
        }

        $data_New = array(  
                        'catename'      => $this->input->post('name'),
                        'status'        => $this->input->post('status')
                     );
        
        $data_Where = array(  
                        'id'    => $id
                     );

        //$insert_data = $this->user->_updateuser($data_New);
        // insert to db
        $insert_data = $this->Generic->editByConditions($data_New, $data_Where , $tablename="tbl_cate"); 

        //print("<pre>".print_r($insert_data,true)."</pre>");die;
        if($insert_data)
            {
                $this->session->set_flashdata('success','Category Info Updated');
                $this->session->set_flashdata('message', 'Category Info Updated');

            }
        else {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Updated Category Info');
        }
        redirect('admin/business/categories');
    }
    // Controller function to delete a specified user
    public function deletecate()
    {
        $this->validateadmin();

        $by_id = $_POST["_id"];
        //$_data = $this->user->_deleteuser($by_id);

        // delete to db
        $_data = $this->db->delete('tbl_cate', array('id'=>$by_id));//$this->Generic->delete($by_id, $tablename="tbl_adminusers");
        if($_data)
        {
            $this->session->set_flashdata('success','success');
            $this->session->set_flashdata('message', 'Category Deleted');
        }
        else 
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('message', 'An error occur when Deleting Category');
        }
        
    }
    

    
}
