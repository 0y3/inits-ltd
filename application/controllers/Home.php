<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Businesses');
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->helper('text');
    }


	public function index($existing_search=0, $page=0)
    {
        $data['icon'] = 'icon.ico';
		$data['nav'] = '';

        $data ['meta_keyword']= 'Creative,INITS,Nigerial,Business Directory Service';
        $data['titel'] = 'INITS Business Directory Service:- Business Category';
        $data['error_page'] = 'included/error_page';

        
        $filterparams = array();
        $query_params = array();
        // Process filter if any was posted
            
        if(($this->input->post() && $existing_search == 0)|| ($this->input->get() && $existing_search == 0))
        {
            // Get all data from the search form

            $filterparams = $this->input->get();
            $this->session->set_userdata(array('search_params' => $filterparams));
            $existing_search = 1;
        }
        else if($existing_search == 1)
        {
            // if there is an existing search it means the parameters are already in the session
            $filterparams = $this->session->search_order_params;
        }
        
        // sanitize params and only pass along the ones with data
        foreach ($filterparams as $key => $value)
        {
            if ($value != '' && $value != NULL && $value != 'all')
                $query_params[$key] = $value;
        }
        
        // Set back any parameter so the filter forms can have the data searched
        $data['filterparams'] = $filterparams;

        // Load all the Biz
        $biz = $this->Businesses->getAll($query_params, $page);
        $data['biz'] = $biz;
        $all_count =  $this->Businesses->getAllCount($query_params);
        $data['counts']=$all_count;
        

        //print("<pre>".print_r( $biz,true)."</pre>");die;
        //load pagenation details
        $this->load->library('pagination');
        $config['base_url'] = site_url("$existing_search");
        $config['total_rows'] = $all_count;
        $config['per_page'] = '25';
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        $data['pagenation'] = $this->pagination->create_links();
        
        //print("<pre>".print_r($maintenance,true)."</pre>");die;

        $data ['page_loader']= 'mainpage/index';
		$this->load->view('mainpage/layout',$data);

        
    }
}
