<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Businesses');
        $this->load->model('utility');
        $this->load->model('Generic');
        $this->load->helper('text');
    }
    
	public function details($slug=null)
    {
        if(!isset($slug)|| empty($slug))
        {
            redirect('');
        }

        $biz_data  = $this->Generic->getByFieldSingle('slug',$slug, $tablename="tbl_biz");// get from db
        if($biz_data)
        {
        	$biz = $this->Businesses->getAll(array('bizslug'=>$slug),$page=0);
        	$data['biz'] = $biz;
			$data['icon'] = 'icon.ico';
			$data['nav'] = '';

	        $data ['meta_keyword']= 'Creative,INITS,Nigerial,Business Directory Service';
	        $data['titel'] = 'INITS Business Directory Service:- Business Category';
	        $data['error_page'] = 'included/error_page';

	        $data ['page_loader']= 'mainpage/business';

			$this->load->view('mainpage/layout',$data);
		}
        else
        {
            redirect('');
        }
	}
}
