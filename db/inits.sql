-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2020 at 12:25 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inits`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_adminusers`
--

CREATE TABLE `tbl_adminusers` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletedat` timestamp NULL DEFAULT NULL,
  `isdeleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_adminusers`
--

INSERT INTO `tbl_adminusers` (`id`, `firstname`, `lastname`, `email`, `phone`, `password`, `status`, `createdat`, `updatedat`, `deletedat`, `isdeleted`) VALUES
(2, 'admin', 'admin', 'admin@admin.com', '08080000000', '$2y$10$YLAfK4oyenDHoe96q6Qs9Olxdv6oXW52dGpMXHTaxdJl/g59lPHJ6', 1, '2020-03-02 13:04:52', '2020-03-02 13:05:07', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_biz`
--

CREATE TABLE `tbl_biz` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `bizname` varchar(255) NOT NULL,
  `description` text,
  `address` text,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deletedat` timestamp NULL DEFAULT NULL,
  `isdeleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_biz`
--

INSERT INTO `tbl_biz` (`id`, `slug`, `bizname`, `description`, `address`, `email`, `phone`, `website`, `status`, `createdat`, `updatedat`, `deletedat`, `isdeleted`) VALUES
(1, 'INITS-Ltd', 'INITS Ltd', 'We have a number of different teams within our company that specialise in different areas of technology and business so you can be sure we develop the best in class solutions that are innovative, practical and timely according to your requirements.', '16 Majaro Street, Onike, Yaba, Lagos', 'info@initsng.com', '08080000000', 'https://www.initsng.com/', 1, '2020-03-02 14:35:16', '2020-03-02 14:35:26', NULL, 0),
(2, 'gems-consulting-company-limited', 'Gems Consulting Company Limited', 'Gems Consulting Company Limited is a Nigerian company with a global approach to business management solution. Driven by the phenomenal contribution that technology has brought into the business landscape, it is by no means surprising that Gems has been in the forefront of the integration of technological solution with business development.', 'Plot, 2D Obasa Rd, Oba Akran, Ikeja', 'info@gems-consult.com', '08169787729', 'https://www.gems-consult.com', 1, '2020-03-02 14:39:33', '2020-03-02 19:43:41', NULL, 0),
(3, 'habile-consulting', 'Habile Consulting', 'A company, abbreviated as co., is a legal entity made up of an association of people,', 'ikeja', 'pncbanking@yahoo.com', '54659809', 'cool', 1, '2020-03-02 19:45:40', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cate`
--

CREATE TABLE `tbl_cate` (
  `id` int(11) NOT NULL,
  `catename` char(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deletedat` timestamp NULL DEFAULT NULL,
  `isdeleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_cate`
--

INSERT INTO `tbl_cate` (`id`, `catename`, `status`, `createdat`, `updatedat`, `deletedat`, `isdeleted`) VALUES
(1, 'web design', 1, '2020-03-02 14:28:25', '2020-03-02 14:29:15', NULL, 0),
(2, 'software development', 1, '2020-03-02 14:29:08', '2020-03-02 14:29:16', NULL, 0),
(3, 'Consultant', 1, '2020-03-02 14:29:12', '2020-03-02 14:29:40', NULL, 0),
(4, 'Publisher', 1, '2020-03-02 14:30:46', NULL, NULL, 0),
(5, 'Communications', 1, '2020-03-02 20:33:25', '2020-03-02 20:45:30', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catebiz`
--

CREATE TABLE `tbl_catebiz` (
  `id` int(11) NOT NULL,
  `cateid` int(11) NOT NULL,
  `bizid` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `createat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateat` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleteat` timestamp NULL DEFAULT NULL,
  `isdeleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_catebiz`
--

INSERT INTO `tbl_catebiz` (`id`, `cateid`, `bizid`, `status`, `createat`, `updateat`, `deleteat`, `isdeleted`) VALUES
(1, 1, 1, 1, '2020-03-02 14:40:58', NULL, NULL, 0),
(2, 2, 1, 1, '2020-03-02 14:41:28', NULL, NULL, 0),
(4, 3, 2, 1, '2020-03-02 19:38:45', NULL, NULL, 0),
(5, 4, 2, 1, '2020-03-02 19:38:45', NULL, NULL, 0),
(6, 3, 3, 1, '2020-03-02 19:45:40', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_impactgallary`
--

CREATE TABLE `tbl_impactgallary` (
  `id` int(11) NOT NULL,
  `impactid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `order_set` int(11) NOT NULL DEFAULT '100',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletedat` timestamp NULL DEFAULT NULL,
  `isdeleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_adminusers`
--
ALTER TABLE `tbl_adminusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_biz`
--
ALTER TABLE `tbl_biz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_catebiz`
--
ALTER TABLE `tbl_catebiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_impactgallary`
--
ALTER TABLE `tbl_impactgallary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_adminusers`
--
ALTER TABLE `tbl_adminusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_biz`
--
ALTER TABLE `tbl_biz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_cate`
--
ALTER TABLE `tbl_cate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_catebiz`
--
ALTER TABLE `tbl_catebiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_impactgallary`
--
ALTER TABLE `tbl_impactgallary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
